var express = require('express')
var router = express.Router()
var fs = require('fs')
var dell_p  = require('./dellprocssing.js')
var formidable = require('formidable')
var csv = require("csvtojson")
var bodyParser = require('body-parser')

comments = []
i = 0


router.use(bodyParser.urlencoded({ extended: false })) 
// parse application/json
router.use(bodyParser.json())

router.use('/public/',express.static('./public/'))

router.get('/',function(req,res){

  res.render('index.html',{comments:comments})
})

router.get('/post',function(req,res){
	res.render('post.html')
})

router.post('/post',function(req,res){
	comment = req.body
	i = i+1
	comment.pid = i
	comments.unshift(comment)
	dell_p.dell_procssing(comment.token,comment.itemID,comment.startPrice,comment.originalPrice)
	res.redirect('/')

})

router.get('/del',function(req,res){
			var comment = req.query
			del_id = comment.pid
			for(var ii = 0; ii < comments.length; ii++) {
			    if(comments[ii].pid.toString() === del_id) {
			        comments.splice(ii, 1);
			        break;
			    }
			}
			res.redirect('/')
})

// this is get method
//app.get('/send',function(req,res){
//	comment = req.query
//	i = i+1
//	comment.pid = i
//	//comments.push(comment) add to last one
//	comments.unshift(comment) // add to frist one			
//	dell_p.dell_procssing(comment.token,comment.itemID,comment.startPrice,comment.originalPrice)
//	res.redirect('/')
//})

router.get('/post_bulk',function(req,res){
	res.render('post_bulk.html')
})

router.post('/upload_bulk',function(req,res){

var comment = {}
			i = i+1

			comment.pid = i
			
			var form = new formidable.IncomingForm()
			form.parse(req, function (err, fields, files) {
				token_dell = fields.token_d
				oldpath = files.file_up.path
			csv().fromFile(oldpath).then(function(jsonObj){ 
			    for (var item in jsonObj)
			    {
			    	console.log(token_dell)
			        console.log(jsonObj[item].itemid)
			        console.log(jsonObj[item].newprice)
			        console.log(jsonObj[item].originalprice)

			        dell_p.dell_procssing(token_dell,jsonObj[item].itemid,jsonObj[item].newprice,jsonObj[item].originalprice)
			    } 
    		  })

			})


			comment.itemID = "Bulk Upload"
			comment.startPrice =''
			comment.originalPrice =''
			//comments.push(comment) add to last one
			comments.unshift(comment) // add to frist one			

			//dell_p.dell_procssing(comment.token,comment.itemID,comment.startPrice,comment.originalPrice)

	res.redirect('/')
	

})

module.exports = router