var mongoose = require('mongoose')

var connection = mongoose.connect('mongodb://3.89.128.59:27017/test',{ useNewUrlParser: true, useFindAndModify: false,useCreateIndex: true, })

var {autoIncrement} = require('mongoose-plugin-autoinc')

var Schema = mongoose.Schema

infoSchema = new Schema({

	formId:{
		type: Number,
    	required: true
	},
	sku:{ 
		type: String
    	//required: true
    },
    ebayTitle:{
        type: String
        //required: true
    },
    price:{ 
        type: Number
        //required: true
    },
    qty:{
        type: Number
        //required: true
    },
    brand:{
        type: String
        //required: true       
    },
    conditionId:{
        type: Number
        //required: true       
    },

    conditionDescription:{
    	type: String
    },

    categoryId:{
        type: Number
        //required: true 
    },   

    ebayImages:{
    	type: String
    	//required: true
    },
    color:{
        type: String
        //required: true
    },
    // Item Specifice
    style:{
    	type: String
    },
    sizeType:{
    	type: String
    },
    sizeWomen:{
    	type: String
    },
    BottomsSize:{
    	type: String
    },

    productType:{
    	type: String
    },

    waist:{
    	type: String
    },
    
    hip:{
    	type: String
    },

    material:{
    	type: String
    },
    garmentCare:{
    	type: String
    },
    occasion:{
    	type: String
    },
    dressLength:{
    	type: String
    },
    sleeveLength:{
    	type: String
    },
    subStyle:{
    	type: String
    },    
    featuredGraphics:{
    	type: String
    },
    length:{
    	type: String
    },
    neckline:{
    	type: String
    },
    lining:{
    	type: String
    },
    countryManufacture:{
    	type: String
    },
    designFeatures:{
    	type: String
    },
    accents:{
    	type: String
    },
    look:{
    	type: String
    },
    packageType:{
    	type: String
    },
    packageLength:{
    	type: Number
    },
    packageWidth:{
    	type: Number
    },
    packageHeight:{
    	type: Number
    },
    weightMajor:{
        type: Number
    },
    weightMinor:{
        type: Number
    },
    postagePolicy:{
        type: String
    },
    returnPolicy:{
        type: String
    },
 // Description
    titleDescription:{
    	type: String
    },
    features1:{
    	type: String
    },
    features2:{
    	type: String
    },
    features3:{
    	type: String
    },
    features4:{
    	type: String
    },
    features5:{
    	type: String
    },

    BrandDescription:{
    	type: String
    },

    ShortDescription:{
    	type: String
    },
    fullDescription:{
        type: String
    },
    descriptionIamge:{
        type: String
    },
    ims:{
        type:Object
    }


})

infoSchema.plugin(autoIncrement, {
  model: 'EbayFormInfo',
  field: 'formId',
  startAt: 1
});
module.exports = mongoose.model('EbayFormInfo', infoSchema)