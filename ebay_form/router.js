var express = require('express')
var router = express.Router()
var info = require('./models/info')
var formidable = require('formidable')
var multer  = require('multer')
var path = require('path')	
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
 
var upload = multer({ storage: storage })


infos = {}




router.get('/',function(req,res){

})

router.get('/new',function(req,res){
	res.render('new.html')
})

/*
router.post('/new',function(req,res){
	var body = req.body
	console.log(body)
	infos = new info(body)
	infos.save(function (err){
		if(err){
		 return	res.send("...something wrong...")
		}
	res.send("... working ...")

	})
	
})*/
/*

router.post('/new',function(req, res){
		upload(req,res,function(err){
			var body = req.body
		if(err){
			return res.send("...no working...")
		}

		image_urls = ""
		
		for(i = 0 ; i < req.files.length; i++)
		{
			req.files[i].path
			if (i != req.files.length -1 )
				image_urls += req.files[i].path + "|"
			else
				image_urls += req.files[i].path
		}
		console.log(image_urls)
		body.ebayImages = image_urls
		console.log(body)

		infos = new info(body)
		infos.save(function (err){
		if(err){
		 return	res.send("...something wrong...")
		}
		res.send("... working ...")

	})


	})




	})

*/

var cpUpload = upload.fields([{ name: 'ebayImages'}, { name: 'descriptionIamge'}])


router.post('/new', cpUpload, function(req, res, next){
	var body = req.body


		image_urls = ""

		one_image_url = req.files['descriptionIamge'][0].path
		
		for(i = 0 ; i < req.files['ebayImages'].length; i++)
		{
			req.files['ebayImages'][i].path
			if (i != req.files.length -1 )
				image_urls += req.files['ebayImages'][i].path + "|"
			else
				image_urls += req.files['ebayImages'][i].path
		}
		console.log(image_urls)
		console.log(one_image_url)
		body.ebayImages = image_urls
		body.descriptionIamge = one_image_url


var html = '<meta name="viewport" content="width=device-width, initial-scale=1.0"> <link rel="stylesheet" type="text/css" href="https://s3-ap-southeast-2.amazonaws.com/bid-for-good/vinnies/style/vini_style.css"><div class="main_content"><div class="warn_ind"></div><section class="top"> <h2 class="top_title">' + body.titleDescription + '</h2> <div class="top_left top_left1 mb_full"> <img src="'+body.descriptionIamge+'"></div> <div class="top_right mb_full"><p><strong>'+body.ShortDescription+'</strong></p> <p><b class="fur_ind">Features</b><br></p> <ul class="ind_med_size">'+body.features1+body.features2+body.features3+body.features4+body.features5+'</ul> <br><br><br> <b class="fur_ind">'+body.brand+'</b><br><p><strong>'+body.BrandDescription+'</strong></p><br><br><img class="he_lo" src="https://s3-ap-southeast-2.amazonaws.com/bid-for-good/vinnies/style/Vinnies-Logo.jpg" alt=" Vinnies Logo"><br><br><b class="fur_ind">St Vincent de Paul Society of Victoria </b> <p>The St Vincent de Paul Society and its wide network of members and volunteers provide practical frontline support advocacy and friendship for the most vulnerable members of our community. Key services include home visitation; Vinnies Shops; youth programs; soup vans; assistance for asylum seekers and refugees; compeer programs for people experiencing mental illness; education and tutoring; and professional accommodation and health services through VincentCare. The St Vincent de Paul Society in Australia has more than 60 000 members and volunteers. Internationally, the Society operates in 149 countries and has over 950 000 members. St Vincent de Paul Society of Victoria is registered as a charity with the Australian Charities and Not-for-profits Commission ABN 28 911 702 061 </p></div><div class="clearfix"></div> </section> <br><br><br>&nbsp; <div class="ctx_footer"> <div class="inner_footer"> </div> <div class="foot_down2"> <div class="term_condition"> <h3 class="h3_color">Terms and Conditions</h3><div class="term"><ol><li>Please include your eBay username as a reference description with payment.</li><li>We will ship the item to the eBay address ONLY. This can be a home address, business address or a P.O. Box address. Please check your shipping address when placing the order. Bid For Good will not be held responsible for items that are shipped to an address you submitted incorrectly.</li><li>Items are picked and packed next business day following receipt of cleared payment.</li><li>Items will be safely packed and sent with appropriate packaging.</li><li>The measurements of the Sale Item are approximate. Due to variances in screen resolutions, devices and screens, the colours of the Sale Item may vary from the image in this listing.</li><li>In the unlikely event that your order is not as described the buyer must contact Bid For Good within 3 days of receipt and can return it once Bid For Good has approved the return and issued a Returns Authority (RA).</li><li><b>Bid for Good &nbsp;</b>is managing the sale of this item on behalf of and at the direction of the St Vincent De Paul Society Victoria ABN 28911702061 a charity registered in the State of VIC.&nbsp;Bid For Good&nbsp;is not responsible for any errors or omissions or for any warranties or guarantees either explicit or implied including the quality, safety, or legality of any aspect of the Item listed.</li><li>Nothing in these terms limits, excludes or modifies or purports to limit, exclude or modify the statutory consumer guarantees as provided under the Competition and Consumer Act, as well as any other implied warranties under the ASIC Act or similar consumer protection laws in the States and Territories of Australia (“Non-Excludable Guarantees”).</li><li>Except for any liability that cannot by law be excluded, including the Non-Excludable Guarantees, Bid For Good (including their respective officers, employees and agents) is not responsible for and excludes all liability (including negligence), for any personal injury; or any loss or damage (including loss of opportunity); whether direct, indirect, special or consequential, arising in any way out of the sales or the supply of the Sale Item (including but not limited to the Buyer’s or any other person’s acceptance/use of the Sale Item).</li><li>No part of the content of this auction or associated pages (including images, video, text, design) may be used without the written permission of the appropriate license holders.</li></ol></div></div> </div> </div> </div> <br> <p class="footer_p"><img src="https://static1.squarespace.com/static/5ab06af796e76f89eeebe5b1/5ae1d281aa4a9989a349154e/5ae1d2bd88251b3e9850552b/1524749053255/BFG-navy-logo-60.png" alt="Bid For Good logo" border="0" align="bottom"><br>Managed by Bid For Good</p>'

		body.fullDescription = html
		console.log(body)
		//infos = new info(body)
		//infos.save(function (err){
		//if(err){
		// return	res.send("...something wrong...")
		//}
		res.send("... working ...")

})



router.get('/edit',function(req,res){
	info.findOne({formId:req.query.formId}, function(err,data){
		if(err){return res.send("...something wrong..")}
			var t_data = data
			//console.log(t_data.ebayImages)
			var getEbayImage = t_data.ebayImages.split('|')			
			t_data.ims = getEbayImage
			console.log(t_data)
			
			

		res.render('edit.html',{info:data})
	})
})

router.post('/edit',function(req,res){
	
})

router.get('/clone',function(req,res){
	
})

router.post('/clone',function(req,res){
	
})

router.get('/del',function(req,res){
	
})

router.post('/del',function(req,res){
	
})
module.exports = router