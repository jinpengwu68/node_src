var http = require('http')

var server = http.createServer()

server.on('request',function(req,res){
	console.log('reciving request'+req.url)

	//ip 地址用来定位计算机
	//端口号定位应用程序

	console.log('client address and port'+ req.socket.remoteAddress + req.socket.remotePort)

	if(req.url ==='/'){res.end('index page')}
	else if(req.url ==='/login'){res.end('login page')}
	else if(req.url ==='/product'){
		var products = [
			{name:'apple',price:8888},
			{name:'banner',price:4000},
			{name:'orange',price:2000}

		]
		//相应内容只能是二进制或者是字符串
		// JSON.parse 字符串数组转数组。 JSON.parse('[one,tow,three]')
		// JSON.stringify 数组转字符串
		res.end(JSON.stringify(products))
	}
	else{res.end('404 not found')}	


})

server.listen(80,function(){
	console.log('start now..')
})
