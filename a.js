// require 可以加载核心模块 也可以加载文件模块 但加载文件模块必须加 "./"
// node 中 没有全局作用域
// 但每个文件模块中都有个对象 ： exports

var fs = require('fs')

console.log('start a...')

var b = require('./b.js')
console.log(b.foo)
console.log(b.foo2)
console.log(b.add(1,33))
//读文件
fs.readFile('./a.js',function(err,data){

    if(err){console.log('something wrong..')}
    else{console.log(data.toString())}

})


console.log('end a...')