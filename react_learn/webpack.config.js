const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')

const htmlPlguin = new HtmlWebPackPlugin({
	template: path.join(__dirname,"./src/index.html"),
	filename:'index.html'
})

module.exports = {
	mode:'development',
	plugins:[
		htmlPlguin
	],
	module:{
        rules:[
        {test:/\.js|jsx$/, use:'babel-loader', exclude: /node_modules/}, //must exclude /node_modules/
        ]
    }
}