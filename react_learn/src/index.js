console.log("ok")
// import  package
import React from 'react' // build componse.
import ReactDOM from 'react-dom' // put on pages

//build DOM elements 
//<h1 id="myh1" title = "this a h1">big text in H1</h1>

const myh1 = React.createElement('h1',{"id":"myh1","title":"this a h1"},'big text in H1')
const testJsx = <div id="mydiv" title="div aaa">{myh1}</div>


ReactDOM.render(testJsx,document.getElementById('app'))