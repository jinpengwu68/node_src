var express = require('express')
var router = express.Router()
var fs = require('fs')
var student = require('./student')
/*
//用 Class 召唤 方法
aa = new student()
aa.find(function(data){
	console.log(data)
})
*/


students = {}

router.get('/', function (req, res) {
	student.find((err, student)=>{
		if(err){
			return res.status(500).send("Server Error")
		}
		res.render('index.html',{students:student})
	}) 
})

/*
 * 渲染添加学生页面
 */
router.get('/students/new', function (req, res) {
	res.render('new.html')
})

/*
 * 处理添加学生
 */
router.post('/students/new', function (req, res) {
	student.save(req.body,(err)=>{
		if(err){res.status(500).end("server.down")}
			res.redirect('/')
	})

})

/*
 * 渲染编辑学生页面
 */
router.get('/students/edit', function (req, res) {
	//res.render('edit.html',{student:{"name":"4893669021","gender":"0","age":"33","hobbies":"IT","id":9}})
	student.findById(parseInt(req.query.id),(err,data)=>{

		if(err){res.status(500).end("server down")}
			console.log(data)
		res.render('edit.html',{student:data})

	})

})

/*
 * 处理编辑学生
 */
router.post('/students/edit', function (req, res) {
	student.updateById(req.body,function(err){
		if(err){res.status(500).end("server down")}
			res.redirect('/')

	})

})
/*
 * 处理删除学生
 */
router.get('/students/delete', function (req, res) {
	student.DeleteById(parseInt(req.query.id),(err)=>{
		if(err){res.status(500).end("server down")}
		res.redirect('/')
	})

})


router.get('/students/clone', function (req, res) {
	student.CloneById(parseInt(req.query.id),(err,)=>{
		if(err){res.status(500).end("server down")}
		res.redirect('/')
	})

})

module.exports = router
