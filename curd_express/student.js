var fs = require('fs')
var db = './db.json'

/*
用 class 必须用 module.exports = class name{} 来外界调用
module.exports = class Animal {
  find(callback){
var data = "Hello world"
callback(data)
}
}
*/
read_file = (callback) => {
	fs.readFile(db,'utf8',(err,data) => {
	if(err){
		return callback(err)	
	}
	callback(null,JSON.parse(data).students)
	})
}

write_file = (dbPath,dbFile,callback)=>{
	fs.writeFile(dbPath,dbFile,function(err){
		if(err){return callback(err)}
			callback(null)
	})
}


exports.find = (callback) => {
	read_file((err,data) => {
		if(err){
			return callback(err)
		}
		callback(null,data)
	})
}

exports.findById = (id,callback) =>{
	read_file((err,data) => {
		if(err){
			return callback(err)
		}

		var ret = data.find(function(item){
			return item.id === id
		})
		callback(null,ret)

})

}

exports.save = (student, callback) =>{
	read_file((err,data) => {
		if(err){
			return callback(err)
		}
		console.log(data.length)
		if(data.length === 0){
			student.id = 1
		}
		else{
			student.id = data[data.length - 1].id + 1
		}
		

		data.push(student)

		var fileData = JSON.stringify({students:data})
		write_file(db,fileData,(err)=>{
			if(err){return callback(err)}
				callback(null)
		})

	})

}

exports.updateById = (student,callback) =>{
	read_file((err,data) => {
		if(err){
			return callback(err)
		}
		student.id = parseInt(student.id)

		var stu = data.find((item)=>{
			return item.id === student.id
		})

		for(var key in student)
			stu[key] = student[key]

		var fileData = JSON.stringify({students:data})

		write_file(db,fileData,(err)=>{
			if(err){return callback(err)}
				callback(null)
		})		
	})

}

exports.DeleteById = (id,callback) =>{

	read_file((err,data) => {
		if(err){
			return callback(err)
		}

		var ret = data.findIndex(function(item){
			return item.id === id
		})

		data.splice(ret,1)

		var fileData = JSON.stringify({students:data})

		write_file(db,fileData,(err)=>{
			if(err){return callback(err)}
				callback(null)
		})
	})	

}

exports.CloneById = (id,callback) =>{

	read_file((err,data) => {
		if(err){
			return callback(err)
		}

		var ret = data.find(function(item){
			return item.id === id
		})

		var new_ret = {}
		for(var key in ret)
			new_ret[key] = ret[key]
		
		
		new_ret.id = data[data.length - 1].id + 1
		console.log(new_ret)
		data.push(new_ret)

		var fileData = JSON.stringify({students:data})

		write_file(db,fileData,(err)=>{
			if(err){return callback(err)}
				callback(null)
		})
	})	

}




