var express = require('express')
var router = express.Router()


router.use('/node_modules/', express.static('./node_modules/'))
router.use('/public/', express.static('./public/'))


router.get('/', function (req, res) {
  res.render('index.html', {
    fruits: [
      '苹果',
      '香蕉',
      '橘子'
    ]
  })
})

module.exports = router
