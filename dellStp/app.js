var http = require('http')
var fs = require('fs')
var template = require('art-template')
var pathurl = require('url')
var dell_p  = require('./dellprocssing.js')
var formidable = require('formidable')
var csv = require("csvtojson")


comments = []
i = 0

http
	.createServer(function(req,res){
		var url = req.url
		var pathobj = pathurl.parse(url, true)
		var pathName = pathobj.pathname
		var pathQuery = pathobj.query
		

		if(pathName === '/'){
			fs.readFile('./views/index.html',function(err,data){
				if(err){return res.end("404")}
				var htmlstr = template.render(data.toString(),{
					comments:comments
				})
					res.end(htmlstr)
			})			
		} else if (pathName == '/send'){
			var comment = pathQuery
			i = i+1
			console.log(i)
			comment.pid = i
			//comments.push(comment) add to last one

			comments.unshift(comment) // add to frist one			

			dell_p.dell_procssing(comment.token,comment.itemID,comment.startPrice,comment.originalPrice)

			res.statusCode = 302 // redericet to url
			res.setHeader('Location','/')
			res.end()

		}else if (pathName == '/upload_bulk'){
			var comment = pathQuery
			i = i+1

			comment.pid = i
			
			var form = new formidable.IncomingForm()
			form.parse(req, function (err, fields, files) {
				token_dell = fields.token_d
				oldpath = files.file_up.path
			csv().fromFile(oldpath).then(function(jsonObj){ 
			    for (var item in jsonObj)
			    {
			    	console.log(token_dell)
			        console.log(jsonObj[item].itemid)
			        console.log(jsonObj[item].newprice)
			        console.log(jsonObj[item].originalprice)

			        dell_p.dell_procssing(token_dell,jsonObj[item].itemid,jsonObj[item].newprice,jsonObj[item].originalprice)
			    } 
    		  })

			})


			comment.itemID = "Bulk Upload"
			comment.startPrice =''
			comment.originalPrice =''
			//comments.push(comment) add to last one
			console.log(comment)
			comments.unshift(comment) // add to frist one			

			//dell_p.dell_procssing(comment.token,comment.itemID,comment.startPrice,comment.originalPrice)

			res.statusCode = 302 // redericet to url
			res.setHeader('Location','/')
			res.end()
			
		}else if (pathName == '/del'){
			var comment = pathQuery
			del_id = comment.pid
			for(var ii = 0; ii < comments.length; ii++) {
			    if(comments[ii].pid.toString() === del_id) {
			        comments.splice(ii, 1);
			        break;
			    }
			}


			res.statusCode = 302 // redericet to url
			res.setHeader('Location','/')
			res.end()

		} else if (pathName === '/post'){

			fs.readFile('./views/post.html',function(err,data){
				if (err){return res.end("404")}
				res.end(data)
			})

		} else if (pathName === '/post_bulk'){

			fs.readFile('./views/post_bulk.html',function(err,data){
				if (err){return res.end("404")}
				res.end(data)
			})

		}else if (pathName.indexOf('/public/')=='0'){
			//console.log(url)
			fs.readFile('.'+url,function(err,data){
				if(err){return res.end("404")}
				res.end(data)
			})
		} else {
			fs.readFile('./views/404.html',function(err,data){
				if(err){return res.end('404')}
				res.end(data)
			})
		}


	})
	.listen(3000,function(){
		console.log("running...")
	})